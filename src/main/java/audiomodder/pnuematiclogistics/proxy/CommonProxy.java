package audiomodder.pnuematiclogistics.proxy;

import audiomodder.pnuematiclogistics.capability.HasPressure;
import audiomodder.pnuematiclogistics.capability.IHasPressure;
import audiomodder.pnuematiclogistics.capability.PressureStorage;
import net.minecraft.item.Item;
import net.minecraftforge.common.capabilities.CapabilityManager;

public class CommonProxy {

    public void registerItemRenderer(Item item, int meta, String id) {
    }

    public void registerCapability() {
        CapabilityManager.INSTANCE.register(IHasPressure.class, new PressureStorage(), new HasPressure.Factory());
    }
}
