package audiomodder.pnuematiclogistics.blocks;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

public class BlockRegistry {

    public static CreativePressureSource creativePressureSource = new CreativePressureSource();
    public static CreativePressureSink creativePressureSink = new CreativePressureSink();
    public static IronPressurePipeBase ironPressurePipeBase = new IronPressurePipeBase();

    public static void register(IForgeRegistry<Block> registry) {
        registry.registerAll(
                creativePressureSource,
                creativePressureSink,
                ironPressurePipeBase
        );

        GameRegistry.registerTileEntity(creativePressureSource.getTileEntityClass(),
                creativePressureSource.getRegistryName().toString());
        GameRegistry.registerTileEntity(creativePressureSink.getTileEntityClass(),
                creativePressureSink.getRegistryName().toString());
        GameRegistry.registerTileEntity(ironPressurePipeBase.getTileEntityClass(),
                ironPressurePipeBase.getRegistryName().toString());
    }

    public static void registerItemBlocks(IForgeRegistry<Item> registry) {
        registry.registerAll(
                creativePressureSource.createItemBlock(),
                creativePressureSink.createItemBlock(),
                ironPressurePipeBase.createItemBlock()
        );
    }

    public static void registerModels() {
        creativePressureSource.registerItemModel(Item.getItemFromBlock(creativePressureSource));
        creativePressureSink.registerItemModel(Item.getItemFromBlock(creativePressureSink));
        ironPressurePipeBase.registerItemModel(Item.getItemFromBlock(ironPressurePipeBase));
    }

}