package audiomodder.pnuematiclogistics.blocks;

import audiomodder.pnuematiclogistics.entity.TileEntityIronPressurePipeBase;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class IronPressurePipeBase extends BlockTileEntity<TileEntityIronPressurePipeBase> {

    TileEntityIronPressurePipeBase tile;

    IronPressurePipeBase() {
        super(Material.ROCK, MapColor.BLACK, "iron_pipe");
    }

    @Override
    public Class<TileEntityIronPressurePipeBase> getTileEntityClass() {
        return TileEntityIronPressurePipeBase.class;
    }

    @Nullable
    @Override
    public TileEntityIronPressurePipeBase createTileEntity(World world, IBlockState state) {
        tile =  new TileEntityIronPressurePipeBase();
        return tile;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }
}
