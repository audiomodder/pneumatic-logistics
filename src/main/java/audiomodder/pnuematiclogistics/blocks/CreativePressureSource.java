package audiomodder.pnuematiclogistics.blocks;

import audiomodder.pnuematiclogistics.PneumaticLogistics;
import audiomodder.pnuematiclogistics.entity.TileEntityCreativePressureSource;
import audiomodder.pnuematiclogistics.gui.PneumaticGuiHandler;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class CreativePressureSource extends BlockTileEntity<TileEntityCreativePressureSource> {

    TileEntityCreativePressureSource tile;

    CreativePressureSource() {
        super(Material.ROCK, MapColor.BLACK, "creative_pressure_source");
    }



    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!worldIn.isRemote) {
            tile = getTileEntity(worldIn, pos);
            playerIn.openGui(PneumaticLogistics.instance, PneumaticGuiHandler.CREATIVE_PRESSURE_SOURCE, worldIn, pos.getX(), pos.getY(), pos.getZ());
        }
  		return true;
    }

    @Override
    public Class<TileEntityCreativePressureSource> getTileEntityClass() {
        return TileEntityCreativePressureSource.class;
    }

    @Nullable
    @Override
    public TileEntityCreativePressureSource createTileEntity(World world, IBlockState state) {
        tile =  new TileEntityCreativePressureSource();
        tile.setTotalPressureSourced(0);
        return tile;
    }
}
