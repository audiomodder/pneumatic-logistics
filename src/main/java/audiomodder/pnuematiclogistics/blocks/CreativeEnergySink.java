package audiomodder.pnuematiclogistics.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraftforge.energy.IEnergyStorage;

public class CreativeEnergySink extends Block implements IEnergyStorage {

    private int totalEnergySunk;

    CreativeEnergySink(Material material, String name) {
        super(material);
        totalEnergySunk = 0;
    }

    @Override
    public int receiveEnergy(int maxReceive, boolean simulate) {
        totalEnergySunk += maxReceive;
        return maxReceive;
    }

    @Override
    public int extractEnergy(int maxExtract, boolean simulate) {
        return 0;
    }

    @Override
    public int getEnergyStored() {
        return 0;
    }

    @Override
    public int getMaxEnergyStored() {
        return 1000000000;
    }

    @Override
    public boolean canExtract() {
        return false;
    }

    @Override
    public boolean canReceive() {
        return true;
    }
}
