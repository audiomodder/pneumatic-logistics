package audiomodder.pnuematiclogistics.blocks;

import audiomodder.pnuematiclogistics.capability.IHasPressure;

public interface BlockPneumatic extends IHasPressure {
}
