package audiomodder.pnuematiclogistics.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraftforge.energy.IEnergyStorage;

public class CreativeEnergySource extends Block implements IEnergyStorage {

    private int totalEnergySourced;

    CreativeEnergySource(Material material, String name) {
        super(material);
        totalEnergySourced = 0;
    }

    @Override
    public int receiveEnergy(int maxReceive, boolean simulate) {
        return 0;
    }

    @Override
    public int extractEnergy(int maxExtract, boolean simulate) {
        totalEnergySourced += maxExtract;
        return maxExtract;
    }

    @Override
    public int getEnergyStored() {
        return 1000000000;
    }

    @Override
    public int getMaxEnergyStored() {
        return 1000000000;
    }

    @Override
    public boolean canExtract() {
        return true;
    }

    @Override
    public boolean canReceive() {
        return false;
    }
}
