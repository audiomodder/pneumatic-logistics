package audiomodder.pnuematiclogistics.blocks;

import audiomodder.pnuematiclogistics.PneumaticLogistics;
import audiomodder.pnuematiclogistics.entity.TileEntityCreativePressureSink;
import audiomodder.pnuematiclogistics.gui.PneumaticGuiHandler;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class CreativePressureSink extends BlockTileEntity<TileEntityCreativePressureSink>  {

    TileEntityCreativePressureSink tile;

    CreativePressureSink() {
        super(Material.ROCK, MapColor.BLACK, "creative_pressure_sink");
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!worldIn.isRemote) {
            tile = getTileEntity(worldIn, pos);
            playerIn.openGui(PneumaticLogistics.instance, PneumaticGuiHandler.CREATIVE_PRESSURE_SINK, worldIn, pos.getX(), pos.getY(), pos.getZ());
        }
        return true;
    }

    @Override
    public Class<TileEntityCreativePressureSink> getTileEntityClass() {
        return TileEntityCreativePressureSink.class;
    }

    @Nullable
    @Override
    public TileEntityCreativePressureSink createTileEntity(World world, IBlockState state) {
        tile =  new TileEntityCreativePressureSink();
        tile.setTotalPressureSunk(0);
        return tile;
    }
}
