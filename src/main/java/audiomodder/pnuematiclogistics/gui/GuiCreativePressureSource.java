package audiomodder.pnuematiclogistics.gui;

import audiomodder.pnuematiclogistics.PneumaticLogistics;
import audiomodder.pnuematiclogistics.blocks.BlockRegistry;
import audiomodder.pnuematiclogistics.entity.TileEntityCreativePressureSource;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

public class GuiCreativePressureSource extends GuiContainer {

    private static final ResourceLocation BG_TEXTURE = new ResourceLocation(PneumaticLogistics.MODID,"textures/gui/blank.png");

    private final TileEntityCreativePressureSource tileEntity;

    public GuiCreativePressureSource(Container container, TileEntityCreativePressureSource tileEntity) {
        super(container);
        this.tileEntity = tileEntity;
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        String name = I18n.format(BlockRegistry.creativePressureSource.getUnlocalizedName() + ".name");
        fontRenderer.drawString(name, xSize / 2 - fontRenderer.getStringWidth(name) / 2, 6, 0x404040);

        fontRenderer.drawString("Pressure sourced:  " + String.valueOf(tileEntity.getTotalPressureSourced()),
                xSize / 2 - fontRenderer.getStringWidth(name) / 2, 15, 0x404040);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1, 1, 1, 1);
        mc.getTextureManager().bindTexture(BG_TEXTURE);
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
    }
}