package audiomodder.pnuematiclogistics.gui;

import audiomodder.pnuematiclogistics.PneumaticLogistics;
import audiomodder.pnuematiclogistics.blocks.BlockRegistry;
import audiomodder.pnuematiclogistics.entity.TileEntityCreativePressureSink;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

public class GuiCreativePressureSink extends GuiContainer {

    private static final ResourceLocation BG_TEXTURE = new ResourceLocation(PneumaticLogistics.MODID,"textures/gui/blank.png");

    private final TileEntityCreativePressureSink tileEntity;

    public GuiCreativePressureSink(Container container, TileEntityCreativePressureSink tileEntity) {
        super(container);
        this.tileEntity = tileEntity;
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        String name = I18n.format(BlockRegistry.creativePressureSink.getUnlocalizedName() + ".name");
        fontRenderer.drawString(name, xSize / 2 - fontRenderer.getStringWidth(name) / 2, 6, 0x404040);

        fontRenderer.drawString("Pressure sunk:  " + String.valueOf(tileEntity.getTotalPressureSunk()),
                xSize / 2 - fontRenderer.getStringWidth(name) / 2, 15, 0x404040);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1, 1, 1, 1);
        mc.getTextureManager().bindTexture(BG_TEXTURE);
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
    }
}