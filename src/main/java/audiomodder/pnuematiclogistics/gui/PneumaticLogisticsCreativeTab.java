package audiomodder.pnuematiclogistics.gui;

import audiomodder.pnuematiclogistics.PneumaticLogistics;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class PneumaticLogisticsCreativeTab extends CreativeTabs {

    public PneumaticLogisticsCreativeTab() {
        super(PneumaticLogistics.MODID);
    }

    @Override
    public ItemStack getTabIconItem() {
        //TODO:  Change this to something that isn't beef
        return new ItemStack(Items.BEEF);
    }
}
