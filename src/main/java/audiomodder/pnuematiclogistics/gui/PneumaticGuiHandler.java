package audiomodder.pnuematiclogistics.gui;

import audiomodder.pnuematiclogistics.container.ContainerCreativePressureSink;
import audiomodder.pnuematiclogistics.container.ContainerCreativePressureSource;
import audiomodder.pnuematiclogistics.entity.TileEntityCreativePressureSink;
import audiomodder.pnuematiclogistics.entity.TileEntityCreativePressureSource;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

import javax.annotation.Nullable;

public class PneumaticGuiHandler implements IGuiHandler {

    public static final int CREATIVE_PRESSURE_SOURCE = 0;
    public static final int CREATIVE_PRESSURE_SINK = 1;

    @Nullable
    @Override
    public Container getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID) {
            case CREATIVE_PRESSURE_SOURCE:
                return new ContainerCreativePressureSource((TileEntityCreativePressureSource)world.getTileEntity(new BlockPos(x, y, z)));
            case CREATIVE_PRESSURE_SINK:
                return new ContainerCreativePressureSink((TileEntityCreativePressureSink)world.getTileEntity(new BlockPos(x, y, z)));
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID) {
            case CREATIVE_PRESSURE_SOURCE:
                return new GuiCreativePressureSource(getServerGuiElement(ID, player, world, x, y, z), (TileEntityCreativePressureSource)world.getTileEntity(new BlockPos(x, y, z)));
            case CREATIVE_PRESSURE_SINK:
                return new GuiCreativePressureSink(getServerGuiElement(ID, player, world, x, y, z), (TileEntityCreativePressureSink) world.getTileEntity(new BlockPos(x, y, z)));
            default:
                return null;
        }
    }
}
