package audiomodder.pnuematiclogistics.capability;

public interface IHasPressure {

    int addPressure(int requestedAmount);

    int removePressure(int requestedAmount);

    int getPressureStored();

    void setPressureStored(int pressure);

    int getMaxPressure();

    boolean canAdd();

    boolean canRemove();
}
