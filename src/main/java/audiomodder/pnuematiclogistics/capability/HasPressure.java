package audiomodder.pnuematiclogistics.capability;

import audiomodder.pnuematiclogistics.entity.AbstractPressureTileEntity;

import java.util.concurrent.Callable;

public class HasPressure implements IHasPressure {

    public static class Factory implements Callable<IHasPressure> {

        @Override
        public IHasPressure call() throws Exception {
            return new HasPressure();
        }
    }

    private static int MAX_PRESSURE = 1000000000;

    private int currentPressure;

    @Override
    public int addPressure(int requestedAmount) {
        if((currentPressure + requestedAmount) < getMaxPressure()) {
            currentPressure += requestedAmount;
            return requestedAmount;
        } else {
            int takenAmount = getMaxPressure() - currentPressure;
            currentPressure = getMaxPressure();
            return takenAmount;
        }
    }

    @Override
    public int removePressure(int requestedAmount) {
        if(currentPressure > requestedAmount) {
            currentPressure -= requestedAmount;
            return requestedAmount;
        } else {
            int takenAmount = currentPressure;
            currentPressure = 0;
            return takenAmount;
        }
    }

    @Override
    public int getPressureStored() {
        return currentPressure;
    }

    @Override
    public void setPressureStored(int pressure) {
        currentPressure = pressure;
    }

    @Override
    public int getMaxPressure() {
        return MAX_PRESSURE;
    }

    @Override
    public boolean canAdd() {
        return true;
    }

    @Override
    public boolean canRemove() {
        return true;
    }
}
