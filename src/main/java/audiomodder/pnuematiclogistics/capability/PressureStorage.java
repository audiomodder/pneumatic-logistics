package audiomodder.pnuematiclogistics.capability;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTPrimitive;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

public class PressureStorage implements Capability.IStorage<IHasPressure> {
    @Nullable
    @Override
    public NBTBase writeNBT(Capability<IHasPressure> capability, IHasPressure instance, EnumFacing side) {
        return new NBTTagInt(instance.getPressureStored());
    }

    @Override
    public void readNBT(Capability<IHasPressure> capability, IHasPressure instance, EnumFacing side, NBTBase nbt) {
        instance.setPressureStored(((NBTPrimitive) nbt).getInt());
    }
}
