package audiomodder.pnuematiclogistics.capability;

import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

public class PressureProvider implements ICapabilitySerializable<NBTBase> {

    @CapabilityInject(IHasPressure.class)
    public static final Capability<IHasPressure> PRESSURE_CAPABILITY = null;

    private IHasPressure instance = PRESSURE_CAPABILITY.getDefaultInstance();

    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing)
    {
        return capability == PRESSURE_CAPABILITY;
    }

    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing)
    {
        return capability == PRESSURE_CAPABILITY ? PRESSURE_CAPABILITY.<T> cast(this.instance) : null;
    }

    @Override
    public NBTBase serializeNBT()
    {
        return PRESSURE_CAPABILITY.getStorage().writeNBT(PRESSURE_CAPABILITY, this.instance, null);
    }

    @Override
    public void deserializeNBT(NBTBase nbt)
    {
        PRESSURE_CAPABILITY.getStorage().readNBT(PRESSURE_CAPABILITY, this.instance, null, nbt);
    }
}
