package audiomodder.pnuematiclogistics.items;

import audiomodder.pnuematiclogistics.PneumaticLogistics;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemBasic extends Item {
    protected String name;

    public ItemBasic(String name) {
        this.name = name;

        setUnlocalizedName(name);
        setRegistryName(name);

        setCreativeTab(PneumaticLogistics.creativeTab);
    }

    public void registerItemModel() {
        PneumaticLogistics.proxy.registerItemRenderer(this, 0, name);
    }

    @Override
    public ItemBasic setCreativeTab(CreativeTabs tab) {
        super.setCreativeTab(tab);
        return this;
    }
}