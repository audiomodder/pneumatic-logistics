package audiomodder.pnuematiclogistics.entity;

import audiomodder.pnuematiclogistics.capability.IHasPressure;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;

public class TileEntityCreativePressureSink extends AbstractPressureTileEntity implements IHasPressure, ITickable {

    private int totalPressureSunk;

    @CapabilityInject(IHasPressure.class)
    public static final Capability<IHasPressure> PRESSURE_CAPABILITY = null;

    private IHasPressure instance = PRESSURE_CAPABILITY.getDefaultInstance();

    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing)
    {
        return capability == PRESSURE_CAPABILITY;
    }

    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing)
    {
        return capability == PRESSURE_CAPABILITY ? PRESSURE_CAPABILITY.<T> cast(this.instance) : null;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setInteger("totalPressureSunk", totalPressureSunk);
        return super.writeToNBT(compound);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        totalPressureSunk = compound.getInteger("totalPressureSunk");
        super.readFromNBT(compound);
    }

    @Override
    public int removePressure(int requestedAmount) {
        return 0;
    }

    @Override
    public int addPressure(int requestedAmount) {
        totalPressureSunk += requestedAmount;

        return requestedAmount;
    }

    @Override
    public int getPressureStored() {
        return 0;
    }

    @Override
    public void setPressureStored(int pressure) {
        totalPressureSunk += pressure;
    }

    @Override
    public int getMaxPressure() {
        return 1000000000;
    }

    @Override
    public boolean canRemove() {
        return false;
    }

    public void setTotalPressureSunk(int totalPressureSunk) {
        this.totalPressureSunk = totalPressureSunk;
    }

    public int getTotalPressureSunk() {
        return totalPressureSunk;
    }

}
