package audiomodder.pnuematiclogistics.entity;

import audiomodder.pnuematiclogistics.capability.IHasPressure;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;

public class TileEntityCreativePressureSource extends AbstractPressureTileEntity implements IHasPressure, ITickable {

    private int totalPressureSourced;

    @CapabilityInject(IHasPressure.class)
    public static final Capability<IHasPressure> PRESSURE_CAPABILITY = null;

    private IHasPressure instance = PRESSURE_CAPABILITY.getDefaultInstance();

    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing)
    {
        return capability == PRESSURE_CAPABILITY;
    }

    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing)
    {
        return capability == PRESSURE_CAPABILITY ? PRESSURE_CAPABILITY.<T> cast(this.instance) : null;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setInteger("totalPressureSourced", totalPressureSourced);
        return super.writeToNBT(compound);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        totalPressureSourced = compound.getInteger("totalPressureSourced");
        super.readFromNBT(compound);
    }

    @Override
    public int removePressure(int requestedAmount) {
        totalPressureSourced += requestedAmount;

        return requestedAmount;
    }

    @Override
    public int addPressure(int requestedAmount) {
        return 0;
    }

    @Override
    public int getPressureStored() {
        return 100;
    }

    @Override
    public void setPressureStored(int pressure) {
        totalPressureSourced += (100 - pressure);
    }

    @Override
    public int getMaxPressure() {
        return 1000000000;
    }

    @Override
    public boolean canAdd() {
        return false;
    }

    public void setTotalPressureSourced(int totalPressureSourced) {
        this.totalPressureSourced = totalPressureSourced;
    }

    public int getTotalPressureSourced() {
        return totalPressureSourced;
    }

}
