package audiomodder.pnuematiclogistics.entity;

import audiomodder.pnuematiclogistics.capability.IHasPressure;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;

public class TileEntityIronPressurePipeBase extends AbstractPressureTileEntity implements IHasPressure, ITickable {

    private int currentPressure;

    @CapabilityInject(IHasPressure.class)
    public static final Capability<IHasPressure> PRESSURE_CAPABILITY = null;

    private IHasPressure instance = PRESSURE_CAPABILITY.getDefaultInstance();

    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing)
    {
        return capability == PRESSURE_CAPABILITY;
    }

    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing)
    {
        return capability == PRESSURE_CAPABILITY ? PRESSURE_CAPABILITY.<T> cast(this.instance) : null;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setInteger("currentPressure", currentPressure);
        return super.writeToNBT(compound);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        currentPressure = compound.getInteger("currentPressure");
        super.readFromNBT(compound);
    }

    @Override
    public int getMaxPressure() {
        return 100;
    }
}
