package audiomodder.pnuematiclogistics.entity;

import audiomodder.pnuematiclogistics.capability.IHasPressure;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class AbstractPressureTileEntity extends TileEntity implements IHasPressure, ITickable
{
    private static int MAX_PRESSURE = 1000000000;
    private int currentPressure;
    private static Logger logger;

    @CapabilityInject(IHasPressure.class)
    public static final Capability<IHasPressure> PRESSURE_CAPABILITY = null;

    private IHasPressure instance = PRESSURE_CAPABILITY.getDefaultInstance();

    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing)
    {
        return capability == PRESSURE_CAPABILITY;
    }

    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing)
    {
        return capability == PRESSURE_CAPABILITY ? PRESSURE_CAPABILITY.<T> cast(this.instance) : null;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setInteger("totalPressureSunk", currentPressure);
        return super.writeToNBT(compound);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        currentPressure = compound.getInteger("totalPressureSunk");
        super.readFromNBT(compound);
    }

    @Override
    public int addPressure(int requestedAmount) {
        if((currentPressure + requestedAmount) < getMaxPressure()) {
            currentPressure += requestedAmount;
            return requestedAmount;
        } else {
            int takenAmount = getMaxPressure() - currentPressure;
            currentPressure = getMaxPressure();
            return takenAmount;
        }
    }

    @Override
    public int removePressure(int requestedAmount) {
        if(currentPressure > requestedAmount) {
            currentPressure -= requestedAmount;
            return requestedAmount;
        } else {
            int takenAmount = currentPressure;
            currentPressure = 0;
            return takenAmount;
        }
    }

    @Override
    public int getPressureStored() {
        return currentPressure;
    }

    @Override
    public void setPressureStored(int pressure) {
        currentPressure = pressure < getMaxPressure() ? pressure : getMaxPressure();
    }

    @Override
    public int getMaxPressure() {
        return MAX_PRESSURE;
    }

    @Override
    public boolean canAdd() {
        return true;
    }

    @Override
    public boolean canRemove() {
        return true;
    }

    @Override
    public void update() {
        getAdjacentTilesWithPressureCapability().entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .forEach(entry -> settlePressureWithTile(entry.getKey()));
    }

    private void settlePressureWithTile(IHasPressure adjacentTile) {
        if((getPressureStored() > adjacentTile.getPressureStored()) && adjacentTile.canAdd() && canRemove()) {
            removePressure(adjacentTile.addPressure(1));
        }
    }

    private Map<IHasPressure, Integer> getAdjacentTilesWithPressureCapability() {
        Map<IHasPressure, Integer> availableTileEntities = new HashMap<>();

        if(doesTileEntityExistWithPressureCapabilityAt(getPos().north(), EnumFacing.SOUTH)) {
            IHasPressure entity = (IHasPressure)getWorld().getTileEntity(getPos().north());
            availableTileEntities.put(entity, entity.getPressureStored());
        }

        if(doesTileEntityExistWithPressureCapabilityAt(getPos().east(), EnumFacing.WEST)) {
            IHasPressure entity = (IHasPressure)getWorld().getTileEntity(getPos().east());
            availableTileEntities.put(entity, entity.getPressureStored());
        }

        if(doesTileEntityExistWithPressureCapabilityAt(getPos().south(), EnumFacing.NORTH)) {
            IHasPressure entity = (IHasPressure)getWorld().getTileEntity(getPos().south());
            availableTileEntities.put(entity, entity.getPressureStored());
        }

        if(doesTileEntityExistWithPressureCapabilityAt(getPos().west(), EnumFacing.EAST)) {
            IHasPressure entity = (IHasPressure)getWorld().getTileEntity(getPos().west());
            availableTileEntities.put(entity, entity.getPressureStored());
        }

        if(doesTileEntityExistWithPressureCapabilityAt(getPos().up(), EnumFacing.DOWN)) {
            IHasPressure entity = (IHasPressure)getWorld().getTileEntity(getPos().up());
            availableTileEntities.put(entity, entity.getPressureStored());
        }

        if(doesTileEntityExistWithPressureCapabilityAt(getPos().down(), EnumFacing.UP)) {
            IHasPressure entity = (IHasPressure)getWorld().getTileEntity(getPos().down());
            availableTileEntities.put(entity, entity.getPressureStored());
        }

        return availableTileEntities;
    }

    private boolean doesTileEntityExistWithPressureCapabilityAt(BlockPos pos, EnumFacing facing) {
        if(getWorld().getTileEntity(pos) == null) {
            return false;
        }
        return getWorld().getTileEntity(pos).hasCapability(PRESSURE_CAPABILITY, facing);
    }
}
