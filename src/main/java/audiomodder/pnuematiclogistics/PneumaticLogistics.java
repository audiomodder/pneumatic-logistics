package audiomodder.pnuematiclogistics;

import audiomodder.pnuematiclogistics.blocks.BlockRegistry;
import audiomodder.pnuematiclogistics.capability.PressureProvider;
import audiomodder.pnuematiclogistics.gui.PneumaticGuiHandler;
import audiomodder.pnuematiclogistics.gui.PneumaticLogisticsCreativeTab;
import audiomodder.pnuematiclogistics.items.ItemRegistry;
import audiomodder.pnuematiclogistics.proxy.CommonProxy;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import org.apache.logging.log4j.Logger;

@Mod(modid = PneumaticLogistics.MODID, name = PneumaticLogistics.NAME, version = PneumaticLogistics.VERSION)
public class PneumaticLogistics
{
    public static final String MODID = "pneumaticlogistics";
    public static final String NAME = "Pneumatic Logistics";
    public static final String VERSION = "0.1";

    public static final PneumaticLogisticsCreativeTab creativeTab = new PneumaticLogisticsCreativeTab();

    private static Logger logger;

    @SidedProxy(serverSide = "audiomodder.pnuematiclogistics.proxy.CommonProxy", clientSide = "audiomodder.pnuematiclogistics.proxy.ClientProxy")
    public static CommonProxy proxy;

    @Mod.Instance(MODID)
    public static PneumaticLogistics instance;

    @Mod.EventHandler
    @java.lang.SuppressWarnings("squid:S2696")
    public void preInit(FMLPreInitializationEvent event)
    {
        logger = event.getModLog(); //NOSONAR
        NetworkRegistry.INSTANCE.registerGuiHandler(this, new PneumaticGuiHandler());
        proxy.registerCapability();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
        // some example code
        logger.info("DIRT BLOCK >> {}", Blocks.DIRT.getRegistryName());
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }

    @Mod.EventBusSubscriber
    public static class RegistrationHandler {

        @SubscribeEvent
        public static void registerItems(RegistryEvent.Register<Item> event) {
            ItemRegistry.register(event.getRegistry());
            BlockRegistry.registerItemBlocks(event.getRegistry());
        }

        @SubscribeEvent
        public static void registerItems(ModelRegistryEvent event) {
            ItemRegistry.registerModels();
            BlockRegistry.registerModels();
        }

        @SubscribeEvent
        public static void registerBlocks(RegistryEvent.Register<Block> event) {
            BlockRegistry.register(event.getRegistry());
        }

        @SubscribeEvent
        public void attachCapability(AttachCapabilitiesEvent<TileEntity> event)
        {
            event.addCapability(new ResourceLocation(MODID, "pressure"), new PressureProvider());
        }

    }
}